#!/bin/bash
#####################################################################
# SCRIPT: EnableSSH.sh
#
# AUTHORS: Joey Tesauro
#
# VERSION 1.5 (2016/03/01)
# Information about this script can be found in the JWT IT Library:
# http://itlib.na.corp.jwt.com/services:scripting
#
# USAGE: 
# Normal: <scriptname>.sh [-v] verbosity [-l] /pathto/logfile.log [-f] local0-7
#####################################################################
 
# Logging Functions
logverb() {
  severity="${2:-5}"
   case "${severity}" in
    1) level="error" ;;
    2) level="warning" ;;
    3) level="notice" ;;
    4) level="info" ;;
    5) level="debug" ;;
  esac
  [[ "${verbosity}" -ge "${severity}" ]] && echo "$(date) (${severity}): ${1}"
  [[ "${verbosity}" -ge "${severity}" ]] && logger -p "${loggingFacility}"."${level}" "(${severity}) - ${1}"
}
logpipe() {
  severity="${1:-5}"
  while read line
  do
    logverb "${line}" "${severity}"
  done
}

# Closes out logging and exits the script with the supplied error code
goodbye() {
  exitcode="${1:-255}"
  case "${exitcode}" in
    0) exitdesc="Success" ;;
    1) exitdesc="Fatal Error" ;;
    2) exitdesc="Logging not configured for this OS" ;;
    255) exitdesc="Undefined Error" ;;
  esac
  logverb "Exiting with exit code ${exitcode}: ${exitdesc}" 5
  logverb "----- End ${0} -----" 3
  exec 2>&-
  exit "${exitcode}"
}
 
# Option Processing
verbosity=5 # Default
logfile="/dev/null" # Default
loggingFacility="local0" # Default
while getopts 'v:l:f:' option
do
  case $option in
    'v') verbosity="${OPTARG}" ;;
    'l') logfile="${OPTARG}" ;;
    'f') loggingFacility="${OPTARG}" ;;
  esac
done

# Platform Distinction
platform=$( uname )
if [[ "${platform}" == "Darwin" ]]; then
  configFile="/etc/syslog.conf"
elif [[ "${platform}" == "Linux" ]]; then
  configFile="/etc/rsyslog.conf"
else
  goodbye 2
fi

# Logging Setup
facilityCheck=($( grep -o "local[0-7]" "${configFile}" ))
facilitySpecific=$( echo "${facilityCheck[@]}" | grep -o "${loggingFacility}" )
if [[ -z "${facilityCheck[@]}" ]]; then
  echo "${loggingFacility}.* ${logfile}" >> "${configFile}"
elif [[ -z "${facilitySpecific}" ]]; then
  echo "${loggingFacility}.* ${logfile}" >> "${configFile}"
else
  sed -i -e "s_${loggingFacility}.*_${loggingFacility}.* ${logfile}_g" "${configFile}"
fi
if [[ "${platform}" == "Darwin" ]]; then
  /bin/launchctl unload /System/Library/LaunchDaemons/com.apple.syslogd.plist
  /bin/launchctl load /System/Library/LaunchDaemons/com.apple.syslogd.plist
elif [[ "${platform}" == "Linux" ]]; then
  service rsyslog restart
else
 goodbye 2
fi

# Welcome Message
logverb "----- ${0} ${*} -----" 3
 
# Pipe all STDERR through logpipe() level 1
exec 2> >(logpipe 1) 
 
# Shift options out so positional parameters are in the correct places
shift $(( OPTIND - 1 ))
 
# Logfile Testing
[[ -f "${logfile}" ]] || touch "${logfile}"
[[ -w "${logfile}" ]] || echo "Error, log file ( ${logfile} ) is not writable." 1>&2
 
# Testing Logging Functions. These can be removed.
logverb "Testing Debug Logging with logverb() function" 5
echo "Testing Debug Logging with logpipe() function" | logpipe 5
echo "Testing capture of STDERR with logpipe() function" >&2
logverb "Testing Log Level 1 - Error" 1
logverb "Testing Log Level 2 - Warning" 2
logverb "Testing Log Level 3 - Summary" 3
logverb "Testing Log Level 4 - Information" 4
logverb "Testing Log Level 5 - Debug" 5
 
# Enable SSH
systemsetup -setremotelogin on

# Create the SSH Access Group
dseditgroup -o create -q com.apple.access_ssh

# Add wppjss to SSH Access group
dseditgroup -o edit -n . -a wppjss -t user com.apple.access_ssh

# Add wppadm to SSH Access group
dseditgroup -o edit -n . -a wppadm -t user com.apple.access_ssh

 
# Goodbye
goodbye 0
